<?php 
$aliases['staging'] = array(
	'uri' => 'project.staging.domain',
	'root' => '/path/to/project-root',
	'remote-host' => 'staging.server.domain',
	'remote-user' => 'staging_server_user',
	'path-aliases' => array(
	      '%drush' => '/path/to/drush',
	      '%drush-script' => '/path/to/drush/drush',
	      '%dump-dir' => '/path/to/drush_dumps/',
	      '%files' => 'sites/project.staging.domain/files',
	     ),
	'databases' => 
		array(
			'default' => 
				array(
					'default' =>
					array(
						'driver' => 'mysql',
					    'database' => 'project_db_name',
					    'username' => 'project_db_name',
					    'password' => 'password',
					    'host' => 'localhost',
					),
				),
		),
	'command-specific' => array (
	       'sql-sync' => array (
	         'no-cache' => TRUE,
	       ),
	     ),
);
?>