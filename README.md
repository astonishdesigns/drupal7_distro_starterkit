# Drupal Distribution Starter Kit #
Use this project as a foundation or starting point for your own
Drupal 7 install profile projects.

# Usage #
1. Clone
2. Remove .git
3. Replace all occurrences of "starterkit" with your *projectname* (must be a single word)
4. git init, start a new repo somewhere and add that URL as the origin to this repository
5. Replace the profile repository URL in distro.make with the your project repository URL
6. Build (use drush make, the easiest way is with Drupal Build Tools)