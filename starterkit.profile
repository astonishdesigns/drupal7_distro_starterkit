<?php
/**
 * Implements HOOK_install_tasks()
 *
 * NOTE: This function must exist before profiler_v2('profile_name')
 * is called!
 */
function starterkit_install_tasks($install_state) {
  include_once('libraries/profiler/profiler_api.inc');

  return array(
    // Run profiler install tasks
    'profiler_install_profile_complete' => array(),

    // Now run custom install tasks
    'input_formats' => array(
     	'display_name' => st('Install Text Formats'),
  		'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
  		'function' => 'starterkit_install_text_formats',
     ),
    'user_roles' => array(
  		'display_name' => st('Install User Roles'),
  		'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
  		'function' => 'starterkit_install_user_roles',
  	),
    'disable_modules' => array(
      'display_name' => st('Disable Standard Modules'),
      'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
      'function' => 'starterkit_disable_modules',
    ),
   );
}

/**
 * Load profiler
 */
!function_exists('profiler_v2') ? require_once('libraries/profiler/profiler.inc') : FALSE;
profiler_v2('starterkit');

function starterkit_install_text_formats() {
  // Add text formats.
	  $filtered_html_format = array(
	    'format' => 'filtered_html',
	    'name' => 'Filtered HTML',
	    'weight' => 0,
	    'filters' => array(
	      // URL filter.
	      'filter_url' => array(
	        'weight' => 0,
	        'status' => 1,
	      ),
	      // HTML filter.
	      'filter_html' => array(
	        'weight' => 1,
	        'status' => 1,
	      ),
	      // Line break filter.
	      'filter_autop' => array(
	        'weight' => 2,
	        'status' => 1,
	      ),
	      // HTML corrector filter.
	      'filter_htmlcorrector' => array(
	        'weight' => 10,
	        'status' => 1,
	      ),
	    ),
	  );
	  $filtered_html_format = (object) $filtered_html_format;
	  filter_format_save($filtered_html_format);

	  $full_html_format = array(
	    'format' => 'full_html',
	    'name' => 'Full HTML',
	    'weight' => 1,
	    'filters' => array(
	      // URL filter.
	      'filter_url' => array(
	        'weight' => 0,
	        'status' => 1,
	      ),
	      // Line break filter.
	      'filter_autop' => array(
	        'weight' => 1,
	        'status' => 1,
	      ),
	      // HTML corrector filter.
	      'filter_htmlcorrector' => array(
	        'weight' => 10,
	        'status' => 1,
	      ),
	    ),
	  );
	  $full_html_format = (object) $full_html_format;
	  filter_format_save($full_html_format);
}

function starterkit_install_user_roles() {
  // Enable default permissions for system roles.
  $filtered_html_permission = filter_permission_name(filter_format_load('filtered_html'));
  user_role_grant_permissions(DRUPAL_ANONYMOUS_RID, array('access content', $filtered_html_permission));
  user_role_grant_permissions(DRUPAL_AUTHENTICATED_RID, array('access content', $filtered_html_permission));

  // Create default roles and assign permissions
  _create_support_role();
}


/**
 * Create support role
 *
 * Creates the "god" role, assigns all permissions to it, and then
 * assigns uid 1 to the new role.
 */
function _create_support_role() {
  $support_role = new stdClass();
  $support_role->name = 'support';
  $support_role->weight = 3;
  user_role_save($support_role);
  user_role_grant_permissions($support_role->rid, array_keys(module_invoke_all('permission')));

  // Set this as the administrator role.
  variable_set('user_admin_role', $support_role->rid);
}

/**
 * Disable standard modules
 *
 * This profile inherits the standard profile settings,
 * but should disable some core modules.
 */
function starterkit_disable_modules() {
  module_disable(array('dashboard', 'overlay', 'shortcut', 'toolbar'));
}
